var ipsum = "Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Nostrud in laboris labore nisi amet do dolor eu fugiat consectetur elit cillum esse. Pariatur occaecat nisi laboris tempor laboris eiusmod qui id Lorem esse commodo in. Exercitation aute dolore deserunt culpa consequat elit labore incididunt elit anim.";

function init() {
    console.log('init');

    console.time('init_delta');

    $('#tab_overview')[0].innerHTML = "Übersicht";
    $('#tab_features')[0].innerHTML = "Leistungen";
    $('#tab_details')[0].innerHTML = "Details";
    $('#tab_faq')[0].innerHTML = "FAQ";
    $('#tab_about')[0].innerHTML = "Über Uns";


    /*overview*/
    $('#overview_card_1_header')[0].innerHTML = "Es geht um Ihren Erfolg";
    $('#overview_card_1_paragraph')[0].innerHTML = "Optimieren Sie den Ertrag Ihres Unternehmens durch den Einsatz" +
        " neuster Technologie und setzen IT-Lösungen ein, die exakt Ihren Wünschen und Anforderungen entspricht.";


    $('#header_title')[0].innerHTML = "celox";
    $('#footer_title')[0].innerHTML = "© " + getYear() + " celox.io - Alle Rechte vorbehalten";

    $('#about_card_1_header_1')[0].innerHTML = "Impressum";
    $('#about_card_1_header_2')[0].innerHTML = "celox";

    var tabel_content = "<table id='table_comp_info' class='mdl-data-table mdl-js-data-table table_company_info'>" +
        "<tbody>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='3'>" +
        "Vertreten durch Martin Pfeffer & Sebastian Grätz</td>" +
        "<td id='table_comp_info_td' class='table_company_info_td'></td>" +
        "</tr>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric'>E-Mail:</td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='3'>" +
        "<a href='mailto:martinpaush@gmail.com'>martinpaush@gmail.com</a></td>" +
        "</tr>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric'>Telefon:</td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='3'>" +
        "<a href='mailto:martinpaush@gmail.com'>+49 151 590 824 65</a></td>" +
        "</tr>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric'>Anschrift:</td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='3'>Weidigweg 17</td>" +
        "</tr>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td'></td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='3'>64297 Darmstadt</td>" +
        "</tr>" +
        "<tr id='table_comp_info_tr'>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric'>Steuernummer:</td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' >------------------------</td>" +
        "<td id='table_comp_info_td' class='mdl-data-table__cell--non-numeric' colspan='2'></td>" +
        "</tr>" +
        "</tbody>" +
        "</table>";

    $('#about_card_1_paragraph_1')[0].innerHTML = tabel_content;

    $('#about_card_2_header_1')[0].innerHTML = "Datenschutz";
    $('#about_card_2_paragraph_1')[0].innerHTML = "Nachfolgend möchten wir Sie über unsere Datenschutzerklärung informieren. Sie finden hier Informationen über die Erhebung und Verwendung persönlicher Daten bei der Nutzung unserer Webseite. Wir beachten dabei das für Deutschland geltende Datenschutzrecht. Sie können diese Erklärung jederzeit auf unserer Webseite abrufen.<br>" +
        "Wir weisen ausdrücklich darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen und nicht lückenlos vor dem Zugriff durch Dritte geschützt werden kann.<br>" +
        "Die Verwendung der Kontaktdaten unseres Impressums zur gewerblichen Werbung ist ausdrücklich nicht erwünscht, es sei denn wir hatten zuvor unsere schriftliche Einwilligung erteilt oder es besteht bereits eine Geschäftsbeziehung. Der Anbieter und alle auf dieser Website genannten Personen widersprechen hiermit jeder kommerziellen Verwendung und Weitergabe ihrer Daten.<br>" +
        "Personenbezogene Daten<br>" +
        "Sie können unsere Webseite ohne Angabe personenbezogener Daten besuchen. Soweit auf unseren Seiten personenbezogene Daten (wie Name, Anschrift oder E-Mail Adresse) erhoben werden, erfolgt dies, soweit möglich, auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. Sofern zwischen Ihnen und uns ein Vertragsverhältnis begründet, inhaltlich ausgestaltet oder geändert werden soll oder Sie an uns eine Anfrage stellen, erheben und verwenden wir personenbezogene Daten von Ihnen, soweit dies zu diesen Zwecken erforderlich ist (Bestandsdaten). Wir erheben, verarbeiten und nutzen personenbezogene Daten soweit dies erforderlich ist, um Ihnen die Inanspruchnahme des Webangebots zu ermöglichen (Nutzungsdaten). Sämtliche personenbezogenen Daten werden nur solange gespeichert wie dies für den geannten Zweck (Bearbeitung Ihrer Anfrage oder Abwicklung eines Vertrags) erforderlich ist. Hierbei werden steuer- und handelsrechtliche Aufbewahrungsfristen berücksichtigt. Auf Anordnung der zuständigen Stellen dürfen wir im Einzelfall Auskunft über diese Daten (Bestandsdaten) erteilen, soweit dies für Zwecke der Strafverfolgung, zur Gefahrenabwehr, zur Erfüllung der gesetzlichen Aufgaben der Verfassungsschutzbehörden oder des Militärischen Abschirmdienstes oder zur Durchsetzung der Rechte am geistigen Eigentum erforderlich ist.<br>" +
        "Datenschutzerklärung für den Webanalysedienst Google Analytics<br>" +
        "Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (\"Google\"). Google Analytics verwendet sog. \"Cookies\", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Wir haben die IP-Anonymisierung aktiviert. Auf dieser Webseite wird Ihre IP-Adresse von Google daher innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: http://tools.google.com/dlpage/gaoptout?hl=de<br>" +
        "Das Speichern von Cookies auf Ihrer Festplatte können Sie verhindern, indem Sie in Ihren Browser-Einstellungen \"keine Cookies akzeptieren\" wählen (Im MS Internet-Explorer unter \"Extras > Internetoptionen > Datenschutz > Einstellung\"; im Firefox unter \"Extras > Einstellungen > Datenschutz > Cookies\"); wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden. Weitere Informationen darüber wie Google Conversion-Daten verwendet sowie die Datenschutzerklärung von Google finden Sie unter: https://support.google.com/adwords/answer/93148?ctx=tltp, http://www.google.de/policies/privacy/<br>";

    $('#about_card_3_header_1')[0].innerHTML = "Haftungsausschluss";
    $('#about_card_3_paragraph_1')[0].innerHTML = "Alle Angaben und Daten wurden nach bestem Wissen erstellt, es wird jedoch keine Gewähr für deren Vollständigkeit und Richtigkeit übernommen.<br>" +
        "Wir behalten uns das Recht vor, ohne vorherige Ankündigung die bereitgestellten Informationen zu ändern, zu ergänzen oder zu entfernen.";

    console.timeEnd('init_delta');
}

